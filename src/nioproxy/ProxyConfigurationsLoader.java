package nioproxy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Загружает конфигурацию из файла, созаёт ProxyConfig
 */
public class ProxyConfigurationsLoader {
    private static final Pattern PROPERTY_KEY_PATTERN = Pattern.compile("(.+?)\\.(.+?)");
    private final String configFilePath;

    ProxyConfigurationsLoader(String configFilePath) {
        this.configFilePath = configFilePath;
    }

    public Collection<ProxyConfig> call() throws InvalidConfigurationException {
        Properties properties = readPropertiesFromFile();
        Map<String, ProxyConfig> configurations = new HashMap<>();

        for (String propertyKey : properties.stringPropertyNames()) {
            Matcher propertyKeyMatcher = PROPERTY_KEY_PATTERN.matcher(propertyKey);
            if (propertyKeyMatcher.matches()) {
                String proxyName = propertyKeyMatcher.group(1);
                String propertyName = propertyKeyMatcher.group(2);

                ProxyConfig proxyConfig = configurations.get(proxyName);
                if (proxyConfig == null) {
                    proxyConfig = new ProxyConfig(proxyName);
                    configurations.put(proxyName, proxyConfig);
                }

                assignConfigProperty(proxyConfig, propertyName, properties.getProperty(propertyKey));
            } else {
                throw new InvalidConfigurationException("Configuration file contains invalid property: " + propertyKey);
            }
        }

        for (ProxyConfig config : configurations.values()) config.assertValid();
        return configurations.values();
    }

    private Properties readPropertiesFromFile() throws InvalidConfigurationException {
        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream(new File(configFilePath)));
        } catch (FileNotFoundException e) {
            throw new InvalidConfigurationException("Configuration file not found at " + configFilePath);
        } catch (IOException e) {
            throw new InvalidConfigurationException("Can't read configuration file at " + configFilePath);
        }

        return properties;
    }

    private void assignConfigProperty(ProxyConfig proxyConfig, String propertyName, String propertyValue)
            throws InvalidConfigurationException {

        try {
            proxyConfig.assignProperty(propertyName, propertyValue);
        } catch (NumberFormatException e) {
            throw new InvalidConfigurationException("Invalid value in " + propertyName);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (NoSuchFieldException e) {
            throw new InvalidConfigurationException(
                    proxyConfig.getProxyName() + " configuration contains unknown property: " + propertyName
            );
        }
    }
}
