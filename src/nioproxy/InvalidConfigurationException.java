package nioproxy;

class InvalidConfigurationException extends Exception {
    InvalidConfigurationException(String message) {
        super(message);
    }
}
