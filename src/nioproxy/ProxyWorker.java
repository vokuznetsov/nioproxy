package nioproxy;

import java.io.IOException;
import java.nio.channels.*;
import java.util.concurrent.BlockingQueue;

/**
 * Создаёт Selector для отслеживания событий на своих сокетах, делегирует их обработку ProxyChannelProcessor
 * Просматривает очередь новых ProxyChannel и подписывается на события по ним.
 */
public class ProxyWorker {
    private final int localPort;
    private final Selector selector;
    private final String name;
    private final BlockingQueue<ProxyChannel> newChannelsQueue;

    ProxyWorker(ProxyConfig proxyConfig, BlockingQueue<ProxyChannel> newChannelsQueue) throws IOException {
        this.name = proxyConfig.getProxyName() + " worker=" + Integer.toHexString(hashCode());
        this.localPort = proxyConfig.getLocalPort();
        this.selector = Selector.open();
        this.newChannelsQueue = newChannelsQueue;
    }

    public void run() {
        ProxyLogger.instance.info("[%s] Worker started", name, localPort);

        try {
            while (!Thread.currentThread().isInterrupted()) {
                selector.select();

                for (SelectionKey key : selector.selectedKeys()) {
                    if (!key.isValid()) continue;

                    ProxyChannel proxyChannel = (ProxyChannel) key.attachment();
                    new ProxyChannelProcessor(key, proxyChannel).call();
                }

                selector.selectedKeys().clear();
                processRegistrationQueue();
            }
        } catch (IOException e) {
            ProxyLogger.instance.error(e, "[%s] I/O error occurred on select", name);
        }
    }

    public void wakeup() {
        selector.wakeup();
    }

    private void processRegistrationQueue() throws ClosedChannelException {
        ProxyChannel proxyChannel = newChannelsQueue.poll();
        if (proxyChannel != null) {
            proxyChannel.clientChannel().register(selector, SelectionKey.OP_READ, proxyChannel);
            proxyChannel.serverChannel().register(selector, SelectionKey.OP_READ, proxyChannel);
        }
    }
}
