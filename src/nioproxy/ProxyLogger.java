package nioproxy;

public class ProxyLogger {
    public static final ProxyLogger instance = new ProxyLogger();

    private static final int ERROR = 1;
    private static final int WARN = 2;
    private static final int INFO = 3;
    private static final int DEBUG = 4;

    private final int level;

    private ProxyLogger() {
        level = INFO;
    }

    public synchronized void error(Exception e) {
        e.printStackTrace(System.err);
    }

    public synchronized void error(Exception e, String format, Object... args) {
        error(format, args);
        e.printStackTrace(System.err);
    }

    public synchronized void error(String format, Object... args) {
        if (level >= ERROR) System.err.printf("ERROR " + format, args).println();
    }

    public synchronized void warn(String format, Object... args) {
        if (level >= WARN) System.out.printf("WARN " + format, args).println();
    }

    public synchronized void info(String format, Object... args) {
        if (level >= INFO) System.out.printf("INFO " + format, args).println();
    }

    public synchronized void debug(String format, Object... args) {
        if (level >= DEBUG) System.out.printf("DEBUG " + format, args).println();
    }
}
