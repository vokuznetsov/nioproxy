package nioproxy;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * Принимает входящие коннекты, подключается к серверу назначения, создаёт ProxyChannel
 */
public class ProxyAcceptorConnector {
    private final ServerSocketChannel acceptorChannel;
    private final InetSocketAddress remoteAddress;
    private final ProxyConfig proxyConfig;

    ProxyAcceptorConnector(ProxyConfig proxyConfig) throws IOException {
        this.acceptorChannel = ServerSocketChannel.open();
        this.acceptorChannel.bind(new InetSocketAddress(proxyConfig.getLocalPort()));
        this.acceptorChannel.configureBlocking(true);
        this.remoteAddress = new InetSocketAddress(proxyConfig.getRemoteHost(), proxyConfig.getRemotePort());
        this.proxyConfig = proxyConfig;
    }

    public ProxyChannel call() {
        SocketChannel clientChannel = accept();
        if (clientChannel == null) return null;
        ProxyLogger.instance.info("[%s] New incoming connection", proxyConfig.getProxyName());

        SocketChannel serverChannel = connect();
        if (serverChannel == null) {
            try {
                clientChannel.close();
            } catch (IOException e) {
                ProxyLogger.instance.error(e, "[%s] Can't close client connection", proxyConfig.getProxyName());
                return null;
            }
        }

        return new ProxyChannel(clientChannel, serverChannel, proxyConfig.getProxyName());
    }

    private SocketChannel accept() {
        try {
            SocketChannel clientChannel = acceptorChannel.accept();
            clientChannel.configureBlocking(false);
            return clientChannel;
        } catch (IOException e) {
            ProxyLogger.instance.error(e, "[%s] Can't accept connection from client", proxyConfig.getProxyName());
            return null;
        }
    }

    private SocketChannel connect() {
        try {
            SocketChannel serverChannel = SocketChannel.open();
            serverChannel.configureBlocking(true);
            if (serverChannel.connect(remoteAddress) && serverChannel.finishConnect()) {
                serverChannel.configureBlocking(false);
                return serverChannel;
            } else {
                ProxyLogger.instance.warn("[%s] Not connected to %s", proxyConfig.getProxyName(), remoteAddress);
              return null;
            }
        } catch (IOException e) {
            ProxyLogger.instance.error(e, "[%s] Can't establish connection to remote server %s",
                    proxyConfig.getProxyName(), remoteAddress);
            return null;
        }
    }
}
