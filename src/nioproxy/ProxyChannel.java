package nioproxy;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Абстракция над парой SocketChannel - clientChannel и serverChannel, а также их общего буфера для обмена данными
 */
class ProxyChannel {
    private static final int BYTE_BUFFER_SIZE = 4096;

    private final SocketChannel clientChannel;
    private final SocketChannel serverChannel;
    private final ByteBuffer buffer;

    private String uid;
    private State state;

    private enum State {
        CLOSED,
        WAIT_FOR_WRITE_TO_CLIENT,
        WAIT_FOR_WRITE_TO_SERVER,
        WAIT_FOR_READ_FROM_ANYWHERE
    }

    ProxyChannel(SocketChannel clientChannel, SocketChannel serverChannel, String proxyName) {
        this.clientChannel = clientChannel;
        this.serverChannel = serverChannel;
        this.state = State.WAIT_FOR_READ_FROM_ANYWHERE;
        this.uid = proxyName + " channel=" + Integer.toHexString(hashCode());
        this.buffer = ByteBuffer.allocate(BYTE_BUFFER_SIZE);
        ProxyLogger.instance.info("[%s] Proxy channel created", uid);
    }

    public SocketChannel clientChannel() {
        return clientChannel;
    }

    public SocketChannel serverChannel() {
        return serverChannel;
    }

    public void close() {
        if (state == State.CLOSED) return;
        state = State.CLOSED;

        try {
            if (clientChannel.isOpen()) clientChannel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (serverChannel.isOpen()) serverChannel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        buffer.clear();
        ProxyLogger.instance.info("[%s] ProxyMain channel closed", uid);
    }

    public boolean isWaitingForReadFromAnywhere() {
        return state == State.WAIT_FOR_READ_FROM_ANYWHERE;
    }

    public boolean isWaitingForWriteToServer() {
        return state == State.WAIT_FOR_WRITE_TO_SERVER;
    }

    public boolean canReadFromClient() {
        return state == State.WAIT_FOR_READ_FROM_ANYWHERE;
    }

    public boolean isWaitingForWriteToClient() {
        return state == State.WAIT_FOR_WRITE_TO_CLIENT;
    }

    public void waitForClientWrite() {
        if (state == State.CLOSED) return;
        if (state != State.WAIT_FOR_WRITE_TO_CLIENT) buffer.flip();
        state = State.WAIT_FOR_WRITE_TO_CLIENT;
    }

    public void waitForServerWrite() {
        if (state == State.CLOSED) return;
        if (state != State.WAIT_FOR_WRITE_TO_SERVER) buffer.flip();
        state = State.WAIT_FOR_WRITE_TO_SERVER;
    }

    public void waitForReadingAvailable() {
        if (state == State.CLOSED) return;
        if (state != State.WAIT_FOR_READ_FROM_ANYWHERE) {
            buffer.flip();
            buffer.clear();
        }
        state = State.WAIT_FOR_READ_FROM_ANYWHERE;
    }

    public void readChunkFromServer() {
        if (!isWaitingForReadFromAnywhere()) return;

        try {
            int bytesRead = serverChannel.read(buffer);
            if (bytesRead < 0) {
                ProxyLogger.instance.info("[%s] Server closed the connection", uid);
                close();
            }
            waitForClientWrite();
        } catch (IOException e) {
            ProxyLogger.instance.error(e, "[%s] Error occurred when reading from server", uid);
            close();
        }
    }

    public void readChunkFromClient() {
        if (!isWaitingForReadFromAnywhere()) return;

        try {
            int bytesRead = clientChannel.read(buffer);
            if (bytesRead < 0) {
                ProxyLogger.instance.info("[%s] Client closed the connection", uid);
                close();
            }
            waitForServerWrite();
        } catch (IOException e) {
            ProxyLogger.instance.error(e, "[%s] Error occurred when reading from client", uid);
            close();
        }
    }
    
    public void writeChunkToServer() {
        if (!isWaitingForWriteToServer()) return;

        try {
            serverChannel.write(buffer);
            if (buffer.remaining() > 0) waitForServerWrite();
            else waitForReadingAvailable();
        } catch (IOException e) {
            ProxyLogger.instance.error(e, "[%s] Error occurred when writing to server", uid);
            close();
        }
    }

    public void writeChunkToClient() {
        if (!isWaitingForWriteToClient()) return;

        try {
            clientChannel.write(buffer);
            if (buffer.remaining() > 0) waitForClientWrite();
            else waitForReadingAvailable();
        } catch (IOException e) {
            ProxyLogger.instance.error(e, "[%s] Error occurred when writing to client", uid);
            close();
        }
    }
}
