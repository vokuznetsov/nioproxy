package nioproxy;

import java.io.IOException;
import java.nio.channels.SelectionKey;

/**
 * ProxyChannelProcessor выполняет над ProxyChannel доступные операции, подписывает сокеты на релевантные состоянию события
 */
public class ProxyChannelProcessor {
    private final SelectionKey key;
    private final ProxyChannel proxyChannel;

    ProxyChannelProcessor(SelectionKey key, ProxyChannel proxyChannel) {
        this.key = key;
        this.proxyChannel = proxyChannel;
    }

    public void call() {
        try {
            if (canReadFromServer()) {
                proxyChannel.readChunkFromServer();
            } else if (canWriteToServer()) {
                proxyChannel.writeChunkToServer();
            } else if (canReadFromClient()) {
                proxyChannel.readChunkFromClient();
            } else if (canWriteToClient()) {
                proxyChannel.writeChunkToClient();
            }
        } finally {
            switchState();
        }
    }

    private boolean canReadFromServer() {
        return proxyChannel.isWaitingForReadFromAnywhere() && key.isReadable() && key.channel().equals(proxyChannel.serverChannel());
    }

    private boolean canWriteToServer() {
        return proxyChannel.isWaitingForWriteToServer() && key.isWritable() && key.channel().equals(proxyChannel.serverChannel());
    }

    private boolean canReadFromClient() {
        return proxyChannel.canReadFromClient() && key.isReadable() && key.channel().equals(proxyChannel.clientChannel());
    }

    private boolean canWriteToClient() {
        return proxyChannel.isWaitingForWriteToClient() && key.isWritable() && key.channel().equals(proxyChannel.clientChannel());
    }

    private void switchState() {
        try {
            if (proxyChannel.isWaitingForReadFromAnywhere()) {
                waitForReadingAvailable();
            } else if (proxyChannel.isWaitingForWriteToClient()) {
                waitForClientWrite();
            } else if (proxyChannel.isWaitingForWriteToServer()) {
                waitForServerWrite();
            } else {
                proxyChannel.clientChannel().keyFor(key.selector()).cancel();
                proxyChannel.serverChannel().keyFor(key.selector()).cancel();
            }
        } catch (IOException e) {
            ProxyLogger.instance.error(e, "Can't register channel(s)");
            proxyChannel.close();
        }
    }

    private void waitForClientWrite() throws IOException {
        proxyChannel.clientChannel().register(key.selector(), SelectionKey.OP_WRITE, proxyChannel);
        proxyChannel.serverChannel().keyFor(key.selector()).cancel();
        proxyChannel.waitForClientWrite();
    }

    private void waitForServerWrite() throws IOException {
        proxyChannel.serverChannel().register(key.selector(), SelectionKey.OP_WRITE, proxyChannel);
        proxyChannel.clientChannel().keyFor(key.selector()).cancel();
        proxyChannel.waitForServerWrite();
    }

    private void waitForReadingAvailable() throws IOException {
        proxyChannel.serverChannel().register(key.selector(), SelectionKey.OP_READ, proxyChannel);
        proxyChannel.clientChannel().register(key.selector(), SelectionKey.OP_READ, proxyChannel);
        proxyChannel.waitForReadingAvailable();
    }
}
