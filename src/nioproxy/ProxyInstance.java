package nioproxy;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Экземпляр прокси-сервера, запускает своих ProxyWorker, которые будут обрабатывать события
 * и ProxyAcceptorConnector, который будет принимать и устанавливать новые коннекты.
 * Для новых коннектов создаётся ProxyChannel и складывается в блокирующую очередь newChannelsQueue, которую разгребают ProxyWorker
 */
public class ProxyInstance {
    private static final int NEW_REGISTRATIONS_LIMIT = 50;

    private final ProxyWorker[] proxyWorkers;
    private final ProxyConfig proxyConfig;
    private final ProxyAcceptorConnector acceptorConnector;
    private final BlockingQueue<ProxyChannel> newChannelsQueue = new LinkedBlockingQueue<>(NEW_REGISTRATIONS_LIMIT);

    ProxyInstance(ProxyConfig proxyConfig) throws IOException {
        ProxyLogger.instance.info("[%s] Initialization", proxyConfig.getProxyName());
        this.proxyConfig = proxyConfig;
        this.proxyWorkers = new ProxyWorker[1];
        this.acceptorConnector = new ProxyAcceptorConnector(proxyConfig);
        createWorkers();
    }

    public void run() {
        startWorkers();
        ProxyLogger.instance.info("[%s] ProxyMain instance started on port %s", proxyConfig.getProxyName(), proxyConfig.getLocalPort());

        try {
            while (!Thread.currentThread().isInterrupted()) {
                ProxyChannel proxyChannel = acceptorConnector.call();
                if (proxyChannel != null) {

                    newChannelsQueue.put(proxyChannel);
                    wakeupWorkers();
                }
            }
        } catch (InterruptedException e) {
            ProxyLogger.instance.error("[%s] Interrupted", proxyConfig.getProxyName());
        }
    }

    private void createWorkers() throws IOException {
        for (int i = 0; i < proxyWorkers.length; i++) {
            proxyWorkers[i] = new ProxyWorker(proxyConfig, newChannelsQueue);
        }
    }

    private void startWorkers() {
        for (ProxyWorker proxyWorker : proxyWorkers) {
            new Thread(proxyWorker::run).start();
        }
    }

    private void wakeupWorkers() {
        for (ProxyWorker proxyWorker : proxyWorkers) {
            if (!newChannelsQueue.isEmpty()) proxyWorker.wakeup();
        }
    }
}
