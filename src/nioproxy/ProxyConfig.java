package nioproxy;

import java.lang.reflect.Field;


/**
 * Конфигурация прокси сервера
 */
public class ProxyConfig {
    private final String proxyName;
    private String remoteHost = null;
    private int remotePort = -1;
    private int localPort = -1;

    ProxyConfig(String proxyName) {
        this.proxyName = proxyName;
    }

    public String getProxyName() {
        return proxyName;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public int getLocalPort() {
        return localPort;
    }

    public void assignProperty(String propertyName, String propertyValue) throws NoSuchFieldException, IllegalAccessException {
        Field field = this.getClass().getDeclaredField(propertyName);

        if (field.getType().getName().equals("int") || field.getType().equals(Integer.class)) {
            field.setInt(this, Integer.parseInt(propertyValue));
        } else if (field.getType().equals(String.class)) {
            field.set(this, propertyValue);
        }
    }

    public void assertValid() throws InvalidConfigurationException {
        if (remoteHost == null) throw new InvalidConfigurationException("Remote host undefined for " + proxyName);
        if (remotePort == -1) throw new InvalidConfigurationException("Remote port undefined for " + proxyName);
        if (localPort == -1) throw new InvalidConfigurationException("Local port undefined for " + proxyName);
    }
}
