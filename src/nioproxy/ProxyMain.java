package nioproxy;

import java.io.IOException;

public class ProxyMain {
    private static final String CONFIG_PATH = "proxy.properties";

    public static void main(String [] args) {
        try {
            for (ProxyConfig proxyConfig : new ProxyConfigurationsLoader(CONFIG_PATH).call()) {
                ProxyInstance proxyInstance = new ProxyInstance(proxyConfig);
                new Thread(proxyInstance::run).start();
            }
        } catch (InvalidConfigurationException | IOException e) {
            ProxyLogger.instance.error(e);
        }
    }
}
